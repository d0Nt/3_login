<?php
/**
 * Created by d0Nt
 * Date: 2018.05.11
 * Time: 21:37
 */

namespace core\Scheduler;

class Job
{
    private $cron;
    function __construct()
    {
        $this->cron = new CronRule();
        $this->scheduler();
    }

    /**
     * Use to set execution schedule
     */
    protected function scheduler(){

    }
    /**
     * Set job execution schedule
     * @return CronRule
     */
    public function schedule(){
        return $this->cron;
    }

    /**
     * Run job
     */
    public function run()
    {
        $jobs_data = "app/runtime_files/jobs.csv";
        if(file_exists($jobs_data)) {
            $content = file_get_contents($jobs_data);
            $jobs = preg_split('/\s+/', $content);
            foreach ($jobs as $job){
                if(strpos($job,get_class($this)) !== false){
                    if(explode(';',$job)[1] > time()) return;
                    else{
                        $content=str_replace($job."\n", "", $content);
                        file_put_contents($jobs_data, $content);
                    }
                }
            }
        }
        else
            file_put_contents($jobs_data, "job;next_run\n");
        file_put_contents($jobs_data, get_class($this).";".$this->cron->nextRun()."\n", FILE_APPEND);
        $this->command();
    }

    /**
     * Job code
     */
    protected function command(){

    }

}