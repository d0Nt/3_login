<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 14:07
 */

namespace core;

use core\Exceptions\Error;

class Controller
{
    private $messages = [];

    protected function getAllMessages(){
        if(Session::get("status_messages") != false){
            if(isset(Session::get("status_messages")["type"]))
                $this->addMessage(Session::get("status_messages"));
            else
                foreach((array)Session::get("status_messages") as $message){
                    $this->addMessage($message);
                }
        }
        Session::set("status_messages", NULL);
        return $this->messages;
    }

    protected function addMessage($text, $type="error"){
        if(is_array($text)){
            array_push($this->messages, ["type" => $text["type"], "text" => $text["text"]]);
            return;
        }
        if(strlen($text) < 1) return;
        array_push($this->messages, ["type" => $type, "text" => $text]);
    }

    public function index(){
        Redirection::Home();
    }

    /**
     * Access restriction to all controller
     * @return bool
     */
    public function canAccess(){
        return true;
    }

    /**
     * Error when access rejected
     */
    public function noAccess(){
        (new Error(401, "no_access"))->printData();
    }
}