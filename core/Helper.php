<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 11:36
 */

namespace core;


class Helper
{
    /**
     * Return url from project without host
     * @return string
     */
    public static function localUrl()
    {
        return trim(substr(urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)),strlen(self::config("app")->directory)), '/');
    }

    /**
     * Faster way to get config("app")->directory
     * @return mixed
     */
    public static function appDir(){
        return self::config("app")->directory;
    }
    /**
     * Return project url address
     * @return string
     */
    public static function host()
    {
        return $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=="https"?"https":"http"."://".$_SERVER['SERVER_NAME'].self::config("app")->directory;
    }

    /**
     * Read and return config file
     * @param $configFile
     * @return bool|object
     */
    public static function config($configFile)
    {
        if(!file_exists(__DIR__ . '/../app/config/' . $configFile . ".php"))
            return false;
        return (object)require __DIR__ . '/../app/config/' . $configFile . ".php";
    }

    /**
     * Get user ip address
     * @param bool $serverSide user only reliable $_SERVER['REMOTE_ADDR'] or not
     * @return mixed|string
     */
    public static function userIP($serverSide = true)
    {
        $addresses = array();
        if(!$serverSide)
        {
            if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            {
                foreach(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']) as $x_f)
                {
                    $addresses[] = trim($x_f);
                }
            }

            if(isset($_SERVER['HTTP_CLIENT_IP']))
            {
                $addresses[] = $_SERVER['HTTP_CLIENT_IP'];
            }

            if (isset($_SERVER['HTTP_X_CLIENT_IP']))
            {
                $addresses[] = $_SERVER['HTTP_X_CLIENT_IP'];
            }

            if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            {
                $addresses[] = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
            }

            if(isset($_SERVER['HTTP_PROXY_USER']))
            {
                $addresses[] = $_SERVER['HTTP_PROXY_USER'];
            }
        }

        if(isset($_SERVER["REMOTE_ADDR"]))
        {
            $addresses[] = $_SERVER["REMOTE_ADDR"];
        }
        foreach ($addresses as $ip)
        {
            if (filter_var($ip, FILTER_VALIDATE_IP))
            {
                return $ip;
            }
        }
        return '';
    }
    public static function userAgent(){
        return $_SERVER["HTTP_USER_AGENT"];
    }
}