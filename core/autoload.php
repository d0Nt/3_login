<?php
/**
 * Created by PhpStorm.
 * User: d0Nt
 * Date: 2017.03.23
 * Time: 14:35
 */
/**
 * Auto loader for classes
 */
spl_autoload_register(function($class) {
    $file = str_replace('\\', '/', $class);

    $file.=".php";
    if(file_exists($file))
        require $file;
});
$config = \core\Helper::config("app");
if(isset($config->time_zone)){
    \core\Init::timeZone($config->time_zone);
}
/**
 * Load twig
 */
require 'Twig/autoloader.php';