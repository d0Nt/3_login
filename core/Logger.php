<?php
/**
 * Created by d0Nt
 * Date: 2018.05.15
 * Time: 21:05
 */

namespace core;


class Logger
{
    public static function errorLog($error, $details){
        $content="~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n";
        $content.="Date: ".date("Y-m-d H:i:s")."\r\n";
        $content.=$error."\r\n";
        $content.=$details."\r\n";
        file_put_contents("app/runtime_files/logs/error_".date("Y-m-d").".log", $content, FILE_APPEND);
    }

    public static function systemLog($system, $log){
        $content="[".date("Y-m-d H:i:s")."] $log\r\n";
        file_put_contents("app/runtime_files/logs/$system.log", $content, FILE_APPEND);
    }
}