<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018.04.03
 * Time: 12:28
 */

namespace core\Database;

class Pagination
{
    private $total_rows = 0;
    /**
     * @var $query Query
     */
    private $query;

    public function numRows(){
        return $this->total_rows;
    }

    function __construct($query)
    {
        $this->query = $query;
        $this->total_rows = Mysql::execute($query, true);
    }
    public function dataItems($perPage = 10, $page = 1){
        $result = new \stdClass();
        if(!is_numeric($perPage) || $perPage < 1)
        {
            $result->data = [];
            $result->error = "Bad per page number";
            return $result;
        }
        if(!is_numeric($page) || $page < 1) {
            $result->data = [];
            $result->error = "Bad page number";
            return $result;
        }
        $result->page       = $page;
        $result->perPage    = $perPage;
        $result->totalRows  = $this->total_rows;
        $result->totalPages = (int)ceil($this->total_rows/$perPage);
        if($result->totalPages<$page){
            $result->data = [];
            $result->error = "Bad per page number";
            return $result;
        }
        if($this->total_rows<1){
            $result->data = [];
            $result->error = "Nothing to show";
            return $result;
        }
        $_query = $this->query;
        $_query->limit((($page-1)*$perPage).", $perPage");
        $res = Mysql::execute($_query);
        if(!isset($res[0])) $result->data[0] = $res;
        else $result->data   = $res;
        return $result;
    }
}