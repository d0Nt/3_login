<?php
/**
 * Created by d0Nt
 * Date: 2018.05.15
 * Time: 15:13
 */

namespace core\Routing;

/**
 * Cronjob route
 */
Route::get("system/cronjobs/run", function(){
    if(!file_exists('app/schedules/')) die("app/schedules/ folder not exist");
    foreach (glob('app/schedules/*.php') as $file)
    {
        $class = str_replace(".php", "", $file);
        $class = str_replace("/", "\\", $class);
        if (class_exists($class)) {
            $job = new $class;
            if(is_subclass_of($job, 'core\\Scheduler\\Job'))
                $job->run();
        }
    }
});
/**
 * Returns image from view directory
 */
Route::get("images/{image}", function($image){
    $image = str_replace("-", "/", $image);
    $file = "app/view/app_resources/images/".$image;
    if(!file_exists($file)) return;
    $fp = fopen($file, 'rb');
    if($fp === false) return;
    header("Content-Type: image/png");
    header("Content-Length: " . filesize($file));
    fpassthru($fp);
    exit;
});
/**
 * Returns style from view directory
 */
Route::get("styles/{style}", function($style){
    $style = str_replace("-", "/", $style);
    header('Content-Type: text/css');
    $file ="app/view/app_resources/css/".$style.".css";

    if(!file_exists($file))
        echo "Failed to load style file $style.css";
    else
        (new \core\View())->renderCss($style);
    exit;
});
/**
 * Returns javascript from view directory
 */
Route::get("scripts/{js}", function($js){
    $js = str_replace("-", "/", $js);
    header('Content-Type: application/javascript');
    $file ="app/view/app_resources/js/".$js.".js";

    if(!file_exists($file))
        echo "Failed to load javascript file $js.js file";
    else
        (new \core\View())->renderJs($js);
    exit;
});