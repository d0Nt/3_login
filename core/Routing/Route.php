<?php
/**
 * Created by d0Nt
 * Date: 2017.06.01
 * Time: 11:07
 */

namespace core\Routing;

use core\Helper;
use core\MethodChecker;
use core\Security;

class Route
{
    private static $pageLoaded = false;

    private static $patterns=[];
    private static $urlOffset = 0;

    public static function isLoaded(){
        return self::$pageLoaded;
    }

    public static function setLoaded(){
        return self::$pageLoaded = true;
    }

    public static function get($route, $function){
        if(self::$pageLoaded) return;
        $patterns = self::$patterns;
        self::$patterns = [];
        //if url without params
        if(preg_match('/\{([^]]+)\}/', $route) === 0)
        {
            if(strcmp(Helper::localUrl(),$route)==0)
            {
                $function();
                self::$pageLoaded = true;
            }
            return;
        }
        else//url with params
        {
            $routeData = explode('/', $route);
            $urlData = explode('/', Helper::localUrl());
            if(count($urlData)!=count($routeData)) return;
            $params = [];
            foreach ($routeData as $key=>$value){
                if(preg_match('/\{([^]]+)\}/', $value) !== 0) {
                    $params[substr($value,1,strlen($value)-2)] = $urlData[$key];
                }
                else if(strcmp($value, $urlData[$key]) !== 0) return;
            }
            foreach ($params as $key=>$value){
                if(!isset($patterns[$key])) continue;
                $pattern = $patterns[$key];
                if(!preg_match("/$pattern/", $value))
                    return;
            }
            if(call_user_func_array($function, $params) === false)
                return;
            self::$pageLoaded = true;
        }
    }

    public static function post($route, $function){
        if(self::$pageLoaded) return;
        $patterns = self::$patterns;
        self::$patterns = [];
        if(preg_match('/\{([^]]+)\}/', $route) === 0)
        {
            if(strcmp(Helper::localUrl(),$route)==0)
            {
                $function();
                self::$pageLoaded = true;
            }
            return;
        }else{
            $routeData = explode('/', $route);
            $params = [];
            $requiredCount = 0;
            foreach ($routeData as $value)
            {
                if(preg_match('/\{([^]]+)\}/', $value) !== 0)
                {
                    $requiredCount++;
                    $key = substr($value, 1, -1);
                    if (isset($_POST[$key]))
                    {
                        $params[$key] = Security::safeInput($_POST[$key]);
                    }
                }
            }
            if(count($params) != $requiredCount) return;
            foreach ($params as $key=>$value){
                if(!isset($patterns[$key])) continue;
                $pattern = $patterns[$key];
                if(!preg_match("/$pattern/", $value))
                    return;
            }
            if(call_user_func_array($function, $params) === false)
                return;
            self::$pageLoaded = true;
        }
    }

    public static function match($patterns){
        self::$patterns = $patterns;
        return new self();
    }

    public static function isAjaxRequest()
    {
        return ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' );
    }

    public static function defaultRoute($defaultController = "index", $defaultFunction = "index"){
        if(self::isLoaded()) return;
        $class = self::findClass(Helper::localUrl(), $defaultController);
        $_controller = new $class();
        if(!$_controller->canAccess()){
            $_controller->noAccess();
            return;
        }
        self::findMethod($_controller, Helper::localUrl(), $defaultFunction);
    }

    private static function controllerClass($controller){
        return "\\app\\controllers\\".$controller."Controller";
    }

    private static function isValidController($controller){
        if (class_exists(self::controllerClass($controller)) &&
            is_subclass_of(self::controllerClass($controller), "core\\Controller"))
            return true;
        return false;
    }

    private static function findClass($url, $controller){
        $urlExplode = explode('/', $url);
        $class=self::controllerClass($controller);
        if(!is_array($urlExplode) || count($urlExplode) < 1) return $class;
        $className = $urlExplode[0];
        if(self::isValidController($className)){
            self::$urlOffset = 1;
            return self::controllerClass($className);
        }
        if(count($urlExplode) > 1) {
            $className = $urlExplode[0] . "\\" . $urlExplode[1];
            if(self::isValidController($className)) {
                self::$urlOffset = 2;
                return self::controllerClass($className);
            }
        }
        self::$urlOffset = 0;
        return $class;
    }

    private static function findMethod($controller, $url, $function){
        $urlExplode = array_slice(explode('/', $url), self::$urlOffset);
        foreach ($urlExplode as $key=>$_function){
            if($_function == "canAccess" || $_function == "noAccess")
                continue;
            if(method_exists($controller, $_function)){
                if(MethodChecker::isStatic($controller,$_function) || !MethodChecker::isPublic($controller,$_function))
                    continue;
                $params_count = MethodChecker::classParamCount($controller, $_function);
                if($params_count < 1){
                    $controller->$_function();
                    return $_function;
                }else{
                    $params = array_slice($urlExplode, $key+1);
                    $params = Security::safeInput($params);
                    if(count($params) != $params_count)
                        continue;
                    call_user_func_array(array($controller, $_function), $params);
                    return $_function;
                }
            }
        }
        $controller->$function();
        return $function;
    }
}