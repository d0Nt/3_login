<?php
/**
 * Created by d0Nt
 * Date: 2018.05.15
 * Time: 16:07
 */

namespace core\Routing;

use core\Helper;
use core\Post;
use core\Session;
use core\View;

class Offline
{
    /**
     * Check if app offline
     * @return bool
     */
    public static function isOffline(){
        $config = Helper::config("app");
        if(isset($config->status) && ($config->status == "offline" || $config->status == "off")) return true;
        else return false;
    }

    /**
     * Check app status and display offline page if app is offline
     */
    public static function checkStatus(){
        $login_error = "";
        if(Post::isRequest() && Post::get("system_password") !== false){
            if(!isset(Helper::config("app")->site_password))
                $login_error = "System password not set";
            else{
                if(Post::get("system_password") == Helper::config("app")->site_password){
                    Session::set("access_password", true);
                }
                else
                    $login_error = "Bad system password";
            }
        }
        $controller = explode("/", Helper::localUrl());
        if(isset($controller[0]))
            $controller=$controller[0];
        else
            $controller = "";
        if(self::isOffline() && Session::get("access_password") == false && !in_array($controller,Helper::config("app")->offline_access_controller)){
            (new View())->render("errors/offline",["title" => "Page offline", "login_error" => isset($login_error)?$login_error:""]);
            exit;
        }
    }
}