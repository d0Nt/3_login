<?php
namespace app\controllers;
use app\models\User;
use core\Controller;
use core\Database\Field;
use core\Post;
use core\Redirection;
use core\Session;
use core\View;

class userController extends Controller
{
    public function index()
    {
        if(!Session::isLogged()){
            Redirection::appDir("user/login");
        }else{
            Redirection::home();
        }
    }

    public function login(){
        if(Post::isRequest()){
            $this->loginUserWith(Post::get("username"), Post::get("password"));
        }
        $view = new View();
        $view->title = "Prisijungimas";
        $view->render("login", ["messages" => $this->getAllMessages()]);
    }

    public function register(){
        if(Post::isRequest()){
            $this->createAccount(Post::get("username"), Post::get("password"));
        }
        $view = new View();
        $view->title = "Registracija";
        $view->render("register", ["messages" => $this->getAllMessages()]);
    }

    public function logout(){
        $user = new User(Session::get("id"));
        $user->logged_in = null;
        $user->save();
        Session::destroy();
        Redirection::appDir("user/login");
    }

    /*
     *
     *
     * vienas vartotojas vienu metu
     * timestamp į hash
     *
     *
     */
    private function loginUserWith($name, $password)
    {
        if (strlen($name) < 3 || strlen($password) < 3) {
            $this->addMessage("Vartotojo vardas arba slaptažodis neteisingas.");
            return;
        }
        $user = User::getByFields(new Field("name", $name));
        if($user == null){
            $this->addMessage("Vartotojo vardas arba slaptažodis neteisingas.");
            return;
        }
        if($user->password !== hash("sha256", $password.$user->salt.($user->last_active))){
            $this->addMessage("Vartotojo vardas arba slaptažodis neteisingas.");
            return;
        }

        if(!is_null($user->logged_in) && time()-strtotime($user->logged_in) < 600){
            $this->addMessage("Vartotojo vardas arba slaptažodis neteisingas.");
            return;
        }
        Session::set("id", $user->id);
        $user->logged_in = date("Y-m-d H:i:s");
        $user->last_active = date('Y-m-d H:i:s');
        $user->password = hash("sha256", $password.$user->salt.($user->last_active));
        $user->save();
        Redirection::home();
        die();
    }

    private function createAccount($name, $password){
        if(strlen($name) < 3){
            $this->addMessage("Vartotojo vardas per trumpas.");
            return;
        }
        if(strlen($name) > 64){
            $this->addMessage("Vartotojo vardas per ilgas.");
            return;
        }
        if(strlen($password) > 128){
            $this->addMessage("Slaptažodis per ilgas.");
            return;
        }
        if(strlen($password) < 3){
            $this->addMessage("Slaptažodis per trumpas");
            return;
        }
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);

        if(!$uppercase) {
            $this->addMessage("Slaptažodyje turi būti bent 1 didžioji raidė.");
            return;
        }
        if(!$lowercase) {
            $this->addMessage("Slaptažodyje turi būti bent viena mažoji raidė.");
            return;
        }
        if(!$number) {
            $this->addMessage("Slaptažodyje turi būti bent 1 skaičius.");
            return;
        }
        $user = User::getByFields(new Field("name", $name));
        if($user != null && $user->isInDatabase()){
            $this->addMessage("Vartotojas su tokiu vardu jau egzistuoja.");
            return;
        }

        $user = new User();
        $user->name = $name;
        $salt = uniqid(mt_rand(), true);
        $user->last_active = date('Y-m-d H:i:s');
        $user->password = hash("sha256", $password.$salt.($user->last_active));
        $user->salt = $salt;
        $user->insert();
        $this->addMessage("Vartotojas sukurtas", "success");
    }
}