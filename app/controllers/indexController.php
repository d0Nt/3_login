<?php
namespace app\controllers;
use app\models\User;
use core\Controller;
use core\Redirection;
use core\Session;
use core\View;

class indexController extends Controller
{
    public function index()
    {
        if(!Session::isLogged()){
            Redirection::appDir("user/login");
            return;
        }
        $view = new View();
        $view->title = "Vartotojo sąsaja";
        $view->render("index",[
            "user" => (new User(Session::get("id")))->getArray()
        ]);
    }

}