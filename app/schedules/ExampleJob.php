<?php
/**
 * Created by d0Nt
 * Date: 2018.05.12
 * Time: 17:21
 */

namespace app\schedules;

use core\Scheduler\Job;

class ExampleJob extends Job
{
    protected function scheduler()
    {
        $this->schedule();
    }

    protected function command()
    {
        echo "job running";
    }
}