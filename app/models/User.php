<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 23:20
 */

namespace app\models;


use core\Model;

class User extends Model
{
    protected static $table = "users";
    protected static $idColumn = "id";
    protected static $selectFields = ["id", "name", "password", "salt", "logged_in", "last_active"];
    protected static $saveFields = ["name", "password", "salt", "logged_in", "last_active"];
}